/*****************************************************************************
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 ****************************************************************************/

#include "../common.h"

#if defined(TARGET_STUB)

typedef struct filestream
{
	stream Stream;
	tchar_t URL[MAXPATH];
	filepos_t Length;
	filepos_t Pos;
	bool_t Silent;
	bool_t Create;

	const tchar_t* Exts;
	bool_t ExtFilter;

} filestream;

static int Get(filestream* p, int No, void* Data, int Size)
{
	int Result = ERR_INVALID_PARAM;
	switch (No)
	{
	case STREAM_URL: GETSTRING(p->URL); break;
	case STREAM_SILENT: GETVALUE(p->Silent,bool_t); break;
	case STREAM_LENGTH: GETVALUECOND(p->Length,int,p->Length>=0); break;
	case STREAM_CREATE: GETVALUE(p->Create,bool_t); break;
	}
	return Result;
}

static int Open(filestream* p, const tchar_t* URL, bool_t ReOpen)
{
	return ERR_NONE;
}

static int Set(filestream* p, int No, const void* Data, int Size)
{
	int Result = ERR_INVALID_PARAM;
	switch (No)
	{
	case STREAM_SILENT: SETVALUE(p->Silent,bool_t,ERR_NONE); break;
	case STREAM_CREATE: SETVALUE(p->Create,bool_t,ERR_NONE); break;
	case STREAM_URL:
		Result = Open(p,(const tchar_t*)Data,0);
		break;
	}
	return Result;
}

static int Read(filestream* p,void* Data,int Size)
{
	return -1;
}

static int ReadBlock(filestream* p,block* Block,int Ofs,int Size)
{
	return Read(p,(char*)(Block->Ptr+Ofs),Size);
}

static int Seek(filestream* p,int Pos,int SeekMode)
{
	return 0;
}

static int Write(filestream* p,const void* Data,int Size)
{
	return -1;
}

static int EnumDir(filestream* p,const tchar_t* URL,const tchar_t* Exts,bool_t ExtFilter,streamdir* Item)
{
	return ERR_NONE;
}

static int Create(filestream* p)
{
	p->Stream.Get = (nodeget)Get,
	p->Stream.Set = (nodeset)Set,
	p->Stream.Read = Read;
	p->Stream.ReadBlock = ReadBlock;
	p->Stream.Write = Write;
	p->Stream.Seek = Seek;
	p->Stream.EnumDir = EnumDir;
	return ERR_NONE;
}

static void Delete(filestream* p)
{

}

static const nodedef File =
{
	sizeof(filestream),
	FILE_ID,
	STREAM_CLASS,
	PRI_MINIMUM,
	(nodecreate)Create,
	(nodedelete)Delete,
};

void File_Init()
{
	NodeRegisterClass(&File);
}

void File_Done()
{
	NodeUnRegisterClass(FILE_ID);
}

stream* FileCreate(const tchar_t* Path)
{
	// create fake stream
	filestream* p = (filestream*)malloc(sizeof(filestream));
	memset(p,0,sizeof(filestream));
	p->Stream.Class = FILE_ID;
	p->Stream.Enum = (nodeenum)StreamEnum;
	Create(p);
	return &p->Stream;
}

void FileRelease(stream* p)
{
	if (p)
	{
		Delete((filestream*)p);
		free(p);
	}
}

bool_t FileExits(const tchar_t* Path)
{
	return 0;
}

int64_t FileDate(const tchar_t* Path)
{
	return 0;
}

#endif
