/*****************************************************************************
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * $Id: str_win32.c 585 2006-01-16 09:48:55Z picard $
 *
 * The Core Pocket Media Player
 * Copyright (c) 2004-2005 Gabor Kovacs
 *
 ****************************************************************************/

#include "../common.h"
#include "../gzip.h"

#define MAXTEXT		20000

#if defined(TARGET_STUB)

void StringFree();

void String_Init()
{

}

void String_Done()
{
	StringFree();
}

#ifndef CP_UTF8
#define CP_UTF8 65001
#endif
void AsciiToTcs(tchar_t* Out,size_t OutLen,const char* In)
{

}

void TcsToAscii(char* Out,size_t OutLen,const tchar_t* In)
{

}

void UTF8ToTcs(tchar_t* Out,size_t OutLen,const char* In)
{

}

void TcsToUTF8(char* Out,size_t OutLen,const tchar_t* In)
{

}

void TcsToStrEx(char* Out,size_t OutLen,const tchar_t* In,int CodePage)
{

}

void StrToTcsEx(tchar_t* Out,size_t OutLen,const char* In,int CodePage)
{

}

void WcsToTcs(tchar_t* Out,size_t OutLen,const uint16_t* In)
{

}

int tcsicmp(const tchar_t* a,const tchar_t* b) 
{
	return 0;
}

int tcsnicmp(const tchar_t* a,const tchar_t* b,size_t n) 
{
 return 0;
}

int GetCodePage(const tchar_t* ContentType)
{
	return 0;
}

#endif
