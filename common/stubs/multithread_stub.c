/*****************************************************************************
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * $Id: multithread_win32.c 343 2005-11-16 20:11:07Z picard $
 *
 * The Core Pocket Media Player
 * Copyright (c) 2004-2005 Gabor Kovacs
 *
 ****************************************************************************/

#include "../common.h"

#if defined(TARGET_STUB)
/*
void* LockCreate()
{
	return NULL;
}

void LockDelete(void* p)
{

}

void LockEnter(void* p)
{

}

void LockLeave(void* p)
{

}

int ThreadPriority(void* Thread,int Priority)
{
	return 0;
}

//bool_t EventWait(void* Handle,int Time) { return WaitForSingleObject(Handle,Time) == WAIT_OBJECT_0; } // -1 = INFINITE
void* EventCreate(bool_t ManualReset,bool_t InitState) { return CreateEvent(NULL,ManualReset,InitState,NULL); }
void EventSet(void* Handle) { SetEvent(Handle); }
void EventReset(void* Handle) { ResetEvent(Handle); }
void EventClose(void* Handle) { CloseHandle(Handle); }

int ThreadId() { return GetCurrentThreadId(); }
*/
void ThreadSleep(int Time) {  }
void ThreadTerminate(void* Handle)
{
}

void* ThreadCreate(int(*Start)(void*),void* Parameter,int Quantum)
{
	return NULL;
}
#endif
