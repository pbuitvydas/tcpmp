/*****************************************************************************
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * $Id: mem_win32.c 430 2005-12-28 14:50:08Z picard $
 *
 * The Core Pocket Media Player
 * Copyright (c) 2004-2005 Gabor Kovacs
 *
 ****************************************************************************/

#include "../common.h"

#if defined(TARGET_STUB)

typedef struct blockgroup
{
	block Block;
	int Mask;
	int Unused; //align to 16bytes

} blockgroup;

void* PhyMemAlloc(int Length,phymemblock* Blocks,int* BlockCount)
{
	void* p = NULL;
	return p;
}

void PhyMemFree(void* p,phymemblock* Blocks,int BlockCount)
{

}

void* PhyMemBeginEx(phymemblock* Blocks,int BlockCount,bool_t Cached)
{
	return NULL;
}

void* PhyMemBegin(uint32_t Phy,uint32_t Length,bool_t Cached)
{
	return NULL;
}

void PhyMemEnd(void* Virt)
{

}

bool_t MemGetInfo(memoryinfo* p)
{
	return 0;
}

void* CodeAlloc(int Size)
{
	void* p;
	return p;
}

void CodeFree(void* Code,int Size)
{

}

void CodeLock(void* Code,int Size)
{

}

void CodeUnlock(void* Code,int Size)
{

}

void CodeFindPages(void* Ptr,uint8_t** PMin,uint8_t** PMax,uintptr_t* PPageSize)
{

}

void CheckHeap()
{

}

size_t AvailMemory()
{
	return 0;
}

void WriteBlock(block* Block,int Ofs,const void* Src,int Length)
{
}

void FreeBlock(block* p)
{

}

bool_t SetHeapBlock(int n,block* Block,int Heap)
{
	return 0;
}

bool_t AllocBlock(size_t n,block* Block,bool_t Optional,int Heap)
{
	void* p;
	return p!=NULL;
}

void* malloc_win32(size_t n)
{
	void* p = NULL;
	return p;
}

void* realloc_win32(void* p,size_t n)
{
	return p;
}

void free_win32(void* p)
{

}

void ShowOutOfMemory()
{
}

void EnableOutOfMemory()
{
}

void DisableOutOfMemory()
{
}

void Mem_Init()
{

}

void Mem_Done()
{
}

#endif
