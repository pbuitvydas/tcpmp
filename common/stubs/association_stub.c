/*****************************************************************************
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 ****************************************************************************/

#include "../common.h"

#if defined(TARGET_STUB)

static void SetURLProtocol(const tchar_t* Base)
{

}
/*
static void SetEditFlags(const tchar_t* Base,DWORD Value)
{

}
*/

static void SetReg(const tchar_t* Base,const tchar_t* New,bool_t State)
{

}

static bool_t CmpReg(const tchar_t* Base, const tchar_t* Value)
{
	bool_t Result = 0;
	return Result;
}

static void SetFileAssociation(const tchar_t* Ext,bool_t State,bool_t MIME,bool_t PlayList)
{

}

static bool_t GetFileAssociation(const tchar_t* Ext,bool_t MIME)
{
	return 0;
}

static int Enum( node* p, int* No, datadef* Param )
{
	return 0;
}

static int Get(node* p, int No, void* Data, int Size)
{
	return 0;
}

static int Set(node* p, int No, const void* Data, int Size)
{
	return ERR_INVALID_PARAM;
}

static void AssignEmpty(node* p)
{

}

static int Create(node* p)
{
	p->Enum = (nodeenum)Enum;
	p->Get = (nodeget)Get;
	p->Set = (nodeset)Set;
	AssignEmpty(p);
	return ERR_NONE;
}

static const nodedef Association =
{
	sizeof(node)|CF_GLOBAL|CF_SETTINGS,
	ASSOCIATION_ID,
	NODE_CLASS,
	PRI_MAXIMUM+500,
	(nodecreate)Create,
};

void Association_Init()
{
	NodeRegisterClass(&Association);
}

void Association_Done()
{
	NodeUnRegisterClass(ASSOCIATION_ID);
}

#endif
