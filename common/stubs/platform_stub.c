/*****************************************************************************
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * $Id: platform_win32.c 622 2006-01-31 19:02:53Z picard $
 *
 * The Core Pocket Media Player
 * Copyright (c) 2004-2005 Gabor Kovacs
 *
 ****************************************************************************/

#include "../common.h"

#if defined(TARGET_STUB)

#ifndef MAX_PATH
#define MAX_PATH 256
#endif

//void DMO_Init();
//void DMO_Done();
void File_Init();
void File_Done();

#define GETVFRAMEPHYSICAL			6144
#define GETVFRAMELEN				6145
#define DBGDRIVERSTAT				6146
#define SETPOWERMANAGEMENT			6147
#define GETPOWERMANAGEMENT			6148

static tchar_t DocumentPath[MAX_PATH]; //important! has to be the MAX_PATH
static tchar_t SystemPath[MAX_PATH]; //important! has to be the MAX_PATH
static bool_t DisplayPower = 1;
static int Orientation = -1;
static stream* Debug = NULL;

void* BrushCreate(rgbval_t val)
{
	return NULL;
}

void BrushDelete(void* val)
{

}

int timeGetTime()
{
	return 0;
}

int GetTimeFreq() 
{ 
	return 1000; 
}
int GetTimeTick()
{
	return timeGetTime();
}
void GetTimeCycle(int* p)
{
	int n=1;
	int j;
	int i = timeGetTime();
	while ((j = timeGetTime())==i)
		++n;
	p[0] = j;
	p[1] = n;
}

void ReleaseModule(void** Module)
{
	if (*Module)
	{
		//FreeLibrary(*Module);
		*Module = NULL;
	}
}

void GetProc(void** Module,void* Ptr,const tchar_t* ProcName,int Optional)
{
}

int DefaultLang()
{
	return LANG_DEFAULT;
}

void PlatformDetect(platform* p)
{
}

void Platform_Init()
{

	File_Init();
	//DMO_Init();
}

void Platform_Done()
{
	File_Done();
	//DMO_Done();
	NodeUnRegisterClass(PLATFORM_ID);
	SetDisplayPower(1,0);
	SetKeyboardBacklight(1);
}

void Log_Done()
{
	if (Debug)
	{
		StreamClose(Debug);
		Debug = NULL;
	}
}

void WinUpdate()
{
}

void WinInvalidate(const rect* Rect, bool_t Local)
{
}

void WinValidate(const rect* Rect)
{
} 

void AdjustOrientation(video* p, bool_t Combine)
{
}

bool_t IsOrientationChanged()
{
	int Old = Orientation;
	Orientation = -1;
	return GetOrientation() != Old;
}

int SetOrientation(int Orientation)
{
	return ERR_NOT_SUPPORTED;
}

bool_t GetHandedness()
{
	return 0;
}

int GetOrientation()
{
	return 0;
}

void QueryDesktop(video* p)
{

}

bool_t GetKeyboardBacklight()
{
	return 0;
}

int SetKeyboardBacklight(bool_t State)
{
	return ERR_NOT_SUPPORTED;
}

bool_t GetDisplayPower()
{
	return DisplayPower;
}

int SetDisplayPower(bool_t State,bool_t Force)
{
	return ERR_NONE;
}

void SleepTimerReset()
{

}

void SleepTimeout(bool_t KeepProcess,bool_t KeepDisplay)
{
}

void _Assert(const char* Exp,const char* File,int Line)
{

}


void WinFill(void* DC,rect* Rect,rect* Exclude,void* Brush)
{

}

void ShowMessage(const tchar_t* Title,const tchar_t* Msg,...)
{

}

void DebugMessage(const tchar_t* Msg,...)
{
}

typedef struct contextreg
{
	int Ofs;
	const tchar_t* Name;
} contextreg;

static contextreg Reg[] = { -1, NULL };


void FindFiles(const tchar_t* Path, const tchar_t* Mask,void(*Process)(const tchar_t*,void*),void* Param)
{

}

void GetModulePath(tchar_t* Path,const tchar_t* Module)
{

}

void GetDebugPath(tchar_t* Path, int PathLen, const tchar_t* FileName)
{
	stprintf_s(Path,PathLen,T("%s\\%s"),DocumentPath,FileName);
}

void GetSystemPath(tchar_t* Path, int PathLen, const tchar_t* FileName)
{
	stprintf_s(Path,PathLen,T("%s\\%s"),SystemPath,FileName);
}

int64_t GetTimeDate()
{
 return 0;
}

bool_t SaveDocument(const tchar_t* Name, const tchar_t* Text,tchar_t* URL,int URLLen)
{

	return 1;
}

int SafeException(void* p)
{

	return 1;
}


void GlobalInvalidate(const rect* Rect)
{

}

void HotKeyToString(tchar_t* Out, size_t OutLen, int HotKey)
{

}

void WaitDisable(bool_t v)
{

}

bool_t WaitBegin()
{
 return 0;
}

void WaitEnd()
{

}

bool_t CheckModule(const tchar_t* Name)
{
	return 0;
}

bool_t HaveDPad()
{
	return 1;
}

#endif
