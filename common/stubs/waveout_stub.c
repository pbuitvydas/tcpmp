/*****************************************************************************
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * $Id: waveout_win32.c 543 2006-01-07 22:06:24Z picard $
 *
 * The Core Pocket Media Player
 * Copyright (c) 2004-2005 Gabor Kovacs
 *
 ****************************************************************************/

#include "../common.h"
#include <math.h>

#if defined(TARGET_STUB)

struct waveout;

typedef struct wavebuffer
{

	planes Planes;
	struct wavebuffer* Next; //next in chain
	struct wavebuffer* GlobalNext;
	tick_t RefTime;
	tick_t EstRefTime;
	int Bytes;
	block Block;

} wavebuffer;

typedef struct waveout
{
	node Node;
	node Timer;
	pin Pin;
	packetformat Input;
	packetformat Output;
	packetprocess Process;

	wavebuffer* Buffers; // global chain
	wavebuffer* FreeFirst;
	wavebuffer* FreeLast;

	int BufferLength; // one buffer length (waveout format)
	tick_t BufferScaledTime; // scaled time of one waveout buffer (BufferLength)
	int BufferScaledAdjust; // waveout bytes to scaled time convert (12bit fixed point)

	int Total; // source format
	int Dropped; // dropped packets

	int Bytes; // output format
	int FillPos;
	int Skip;
	wavebuffer* FillFirst;
	wavebuffer* FillLast;
	wavebuffer** Pausing;
	tick_t FillLastTime;

	void* PCM;
	int PCMSpeed;
	int BufferLimit;
	int BufferLimitFull;
	tick_t Tick;
	int TimeRef;

	bool_t Play;
	fraction Speed;
	fraction SpeedTime;
	int AdjustedRate;

	bool_t Dither;
	bool_t BufferMode;
	int Stereo;
	int Quality;

	bool_t ForcePriority;
	bool_t SoftwareVolume;
	bool_t MonoVol;
	bool_t Mute; 
	int PreAmp;
	int VolumeDev;	// backup value when mute is turned on
	int VolumeSoft;
	int VolumeSoftLog;
	int VolumeRamp;

	int BenchCurrSum;
	int BenchAvg;
	int BenchAdj;
	size_t BenchAvgLimit;
	int BenchSpeedAvg;
	int BenchWaitPos;
	
} waveout;

#define WAVEOUT(p) ((waveout*)((char*)(p)-OFS(waveout,Timer)))

static void Pause(waveout* p);
static void Write(waveout* p, tick_t CurrTime);

static int SetVolumeSoft(waveout* p,int v,bool_t m)
{


	return ERR_NONE;
}

static int GetVolume(waveout* p)
{

	return 0;
}

static tick_t Time(waveout* p)
{
	return 0;
}

static int TimerGet(void* pt, int No, void* Data, int Size)
{
	waveout* p = WAVEOUT(pt);
	int Result = ERR_INVALID_PARAM;
	return Result;
}

static int Get(waveout* p, int No, void* Data, int Size)
{
	int Result = ERR_INVALID_PARAM;
	switch (No)
	{
	case OUT_INPUT: GETVALUE(p->Pin,pin); break;
	case OUT_INPUT|PIN_FORMAT: GETVALUE(p->Input,packetformat); break;
	case OUT_INPUT|PIN_PROCESS: GETVALUE(p->Process,packetprocess); break;
	case OUT_OUTPUT|PIN_FORMAT: GETVALUE(p->Output,packetformat); break;
	case OUT_TOTAL:GETVALUE(p->Total,int); break;
	case OUT_DROPPED:GETVALUE(p->Dropped,int); break;
	case AOUT_VOLUME: GETVALUE(GetVolume(p),int); break;
	case AOUT_MUTE: GETVALUE(p->Mute,bool_t); break;
	case AOUT_PREAMP: GETVALUE(p->PreAmp,int); break;
	case AOUT_STEREO: GETVALUE(p->Stereo,int); break; 
	case AOUT_MODE: GETVALUE(p->BufferMode,bool_t); break;
	case AOUT_QUALITY: GETVALUE(p->Quality,int); break;
	case AOUT_TIMER: GETVALUE(&p->Timer,node*); break;
	}
	return Result;
}

static void UpdateBenchAvg(waveout* p)
{

}

static void ReleaseBuffer(waveout* p,wavebuffer* Buffer,bool_t UpdateTick)
{

}

static void Reset(waveout* p)
{

}

static wavebuffer* GetBuffer(waveout* p)
{
	wavebuffer* Buffer;
	return Buffer;
}

static void Write(waveout* p, tick_t CurrTime)
{

}

static int Send(waveout* p, const constplanes Planes, int Length, tick_t RefTime, tick_t CurrTime, int Speed)
{
	return ERR_NONE;
}

static int UpdatePCM(waveout* p,const audio* InputFormat)
{
	return ERR_NONE;
}

static int UpdateBufferTime(waveout* p)
{
	return ERR_NONE;
}

static int Process(waveout* p,const packet* Packet,const flowstate* State)
{
	return Send(p,Packet->Data,Packet->Length,Packet->RefTime,State->CurrTime,p->PCMSpeed);
}

static bool_t FreeBuffers(waveout* p);

static int UpdateInput(waveout* p)
{
	return ERR_NONE;
}

static int Update(waveout* p)
{
	return ERR_NONE;
}

static bool_t FreeBuffers(waveout* p)
{
	wavebuffer** Ptr;
	wavebuffer* Buffer;
	bool_t Changed = 0;
	return Changed;
}

static int Hibernate(waveout* p,int Mode)
{
	bool_t Changed = 0;
	return Changed ? ERR_NONE : ERR_OUT_OF_MEMORY;
}

static void Pause(waveout* p)
{

}

static int UpdatePlay(waveout* p)
{
	return ERR_NONE;
}

static int SetVolumeDev(waveout* p,int v)
{
	return ERR_NONE;
}

static int TimerSet(void* pt, int No, const void* Data, int Size)
{

	return 0;
}

static void UpdateSoftwareVolume(waveout* p)
{

}

static int UpdatePreAmp(waveout* p)
{

	return ERR_NONE;
}

static int Set(waveout* p, int No, const void* Data, int Size)
{
	int Result = ERR_INVALID_PARAM;
	return Result;
}

static int Create(waveout* p)
{
	return ERR_NONE;
}

static void Delete(waveout* p)
{
	PacketFormatClear(&p->Input);
	PCMRelease(p->PCM);
}

static const nodedef WaveOut =
{
	sizeof(waveout)|CF_GLOBAL,
	WAVEOUT_ID,
	AOUT_CLASS,
	PRI_DEFAULT,
	(nodecreate)Create,
	(nodedelete)Delete,
};

void WaveOut_Init()
{
	NodeRegisterClass(&WaveOut);
}

void WaveOut_Done()
{
	NodeUnRegisterClass(WAVEOUT_ID);
}

#endif
